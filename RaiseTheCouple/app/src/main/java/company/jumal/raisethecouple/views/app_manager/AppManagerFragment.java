package company.jumal.raisethecouple.views.app_manager;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import company.jumal.raisethecouple.R;
import company.jumal.raisethecouple.views.base.BaseFragment;

/**
 * Created by Administrator on 2018-02-18.
 * 앱 매니저
 */

public class AppManagerFragment extends BaseFragment
{
    @Override
    protected View uiInit(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        v_Container = inflater.inflate(R.layout.fragment_app_manager, container, false);

        return v_Container;
    }

    @Override
    protected void onBackPressed() {

    }
}
