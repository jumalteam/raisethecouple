package company.jumal.raisethecouple.utils.util;

import android.os.Handler;
import android.os.Message;

/**
 * Created by seeroo_dev on 2018. 3. 20..
 */

public class HandleUtil {
    private static final String TAG = HandleUtil.class.getSimpleName();

    /**
     * 핸들러를 받아서 메시지를 보낸다.
     * @param reqHandle
     * @param reqType
     */
    public static void sendMessage(Handler reqHandle, int reqType){
        Message msg = new Message();
        msg.what = reqType;
        reqHandle.sendMessage(msg);
    }


}
