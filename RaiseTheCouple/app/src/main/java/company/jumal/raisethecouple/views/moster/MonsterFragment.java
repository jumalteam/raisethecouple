package company.jumal.raisethecouple.views.moster;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import company.jumal.raisethecouple.R;
import company.jumal.raisethecouple.services.AlwaysOnTopService;
import company.jumal.raisethecouple.utils.L;
import company.jumal.raisethecouple.utils.manager.permission.PermissionManager;
import company.jumal.raisethecouple.views.base.BaseFragment;

import static company.jumal.raisethecouple.utils.manager.permission.PermissionManager.REQ_PERMISSION_CODE_SYSTEM_ALERT_WINDOW;

/**
 * Created by Administrator on 2018-02-18.
 * 몬스터 화면
 */

public class MonsterFragment extends BaseFragment
{
    public static final String TAG = MonsterFragment.class.getSimpleName();


    @Override
    protected View uiInit(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        v_Container = inflater.inflate(R.layout.fragment_monster, container, false);
        onDrawLayout();

        return v_Container;
    }

    @Override
    protected void onBackPressed() {

    }


    private void onDrawLayout(){
        L.d("---------------------");

        v_Container.findViewById(R.id.bt_main_01).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // 오버레이 권한 체크
                if(PermissionManager.getInstance().checkOverlayWindowService(getActivity())){

                    //권한 있음
                    startService();
                }else{
                    //권한 없음

                    //권한요청 dialog
                    setOverLayPermissionDialog();
                }
            }


        });

        v_Container.findViewById(R.id.bt_main_02).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                stopService();
                finishProcess();
            }
        });
    }


    private void startService()
    {
        L.d("startservice");
        try
        {
            m_AC_Parent.startService(new Intent(m_AC_Parent, AlwaysOnTopService.class));
        }
        catch (Exception e)
        {
            L.e(e.toString());
        }
    }

    private void stopService()
    {
        L.d("stop service");
        try
        {
            m_AC_Parent.stopService(new Intent(m_AC_Parent, AlwaysOnTopService.class));
        }
        catch (Exception e)
        {
            L.e(e.toString());
        }
    }




    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode)
        {
            case REQ_PERMISSION_CODE_SYSTEM_ALERT_WINDOW:
                //사용자가 권한을 수락함
                //if(resultCode == RESULT_OK )
                if(Settings.canDrawOverlays(getActivity()))
                {
                    // overlay 서비스 start
                    startService();
                }
                break;

            default:
                break;
        }
    }


    /**
     * OverLayWindow 권한을 받아온다.
     */
    private void setOverLayPermissionDialog()
    {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(m_AC_Parent);
        dialogBuilder.setMessage(R.string.dialog_req_permission);
        dialogBuilder.setTitle(R.string.app_name);
        dialogBuilder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //- 권한 요청을 위한 설정 화면으로 이동
                 PermissionManager.getInstance().onObtainingPermissionOverlayWindow(m_AC_Parent);
            }
        });
        dialogBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //- 감히 권한 요청을 거절해 ?
                Toast.makeText(getActivity(), R.string.dialog_req_permission_cancel, Toast.LENGTH_SHORT).show();
                finishProcess();
            }
        });
        AlertDialog alert = dialogBuilder.create();
        alert.show();
    }


    private void finishProcess()
    {
        L.d("");
    }


}
