package company.jumal.raisethecouple.viewmodels;

import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import company.jumal.raisethecouple.interfaces.LoginResultCallback;
import company.jumal.raisethecouple.models.User;

public class LoginViewModel extends ViewModel
{
    private LoginResultCallback callback  =   null;
    private User    user    =   null;

    public LoginViewModel(LoginResultCallback _callback)
    {
        callback  =   _callback;
        user    =   new User("",    "", "", "", "", "");
    }

    public TextWatcher getEmailTextWatcher()
    {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                user.setMail(s.toString());
            }
        };
    }

    public TextWatcher getPasswordTextWhatcher()
    {
        return new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                user.setPw(s.toString());
            }
        };
    }

    public void loginClicked(@NonNull View view)
    {
        isDataValidity();
    }

    private void isDataValidity()
    {
        if(user.isInputDataValid() == true)
            callback.onSuccess("Login was successful");
        else
            callback.onError("Email or Password not valid");



    }
}
