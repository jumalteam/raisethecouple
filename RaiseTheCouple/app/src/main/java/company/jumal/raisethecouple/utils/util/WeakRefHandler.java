package company.jumal.raisethecouple.utils.util;

import android.os.Handler;
import android.os.Message;

import java.lang.ref.WeakReference;

/**
 * Created by seeroo_dev on 2018. 3. 12..
 */

public class WeakRefHandler extends Handler{
    private final WeakReference<IOnHandlerMessage> mHandlerActivity;

    public WeakRefHandler(IOnHandlerMessage activity){
        mHandlerActivity = new WeakReference<IOnHandlerMessage>(activity);
    }

    @Override
    public void handleMessage(Message msg){
        super.handleMessage(msg);
        IOnHandlerMessage activity = (IOnHandlerMessage) mHandlerActivity.get();
        if(activity == null) return;

        activity.handlerMessage(msg);
    }


}
