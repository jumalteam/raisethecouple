package company.jumal.raisethecouple.viewmodels;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

import company.jumal.raisethecouple.interfaces.LoginResultCallback;

public class LoginViewModelFactory extends ViewModelProvider.NewInstanceFactory
{
    private LoginResultCallback m_Callback    =   null;

    public LoginViewModelFactory(LoginResultCallback _callback)
    {
        m_Callback = _callback;
    }

    @Override
    public <T extends ViewModel> T create(Class<T> modelcalss)
    {
        return (T) new LoginViewModel(m_Callback);
    }

}
