package company.jumal.raisethecouple.utils.util;

import android.os.Handler;
import android.os.Looper;

import java.util.Timer;
import java.util.TimerTask;

import company.jumal.raisethecouple.utils.Constant;
import company.jumal.raisethecouple.utils.L;

/**
 * Created by seeroo_dev on 2018. 3. 16..
 */

public class ChaTimer{

    private static final int DEFAULT_DELAY_TIME = 10;
    private static final int DEFAULT_PERIOD = 1000;
    private static final int DEFAULT_STATE_TIME = 7;    //액션 타임

    private Handler mManChaHandler;

    private Timer mTimer;
    private int mDelayTime = 0;
    private int mTimerCount = 0;

    public ChaTimer(Handler handler){
        initChaTimer(handler, DEFAULT_DELAY_TIME);
    }

    public ChaTimer(Handler handler, int delayTime){
        initChaTimer(handler, delayTime);
    }

    private void initChaTimer(Handler handler, int delayTime){
        L.d("initChaTimer");
        this.mTimer = new Timer();
        this.mManChaHandler = handler;
        mDelayTime = delayTime;
    }


    public void startTimer(){
        L.d("startTimer");
        if(mTimer == null){
            mTimer = new Timer();
        }
        mTimer.schedule(new MainTimerTask(), mDelayTime, DEFAULT_PERIOD);
    }

    public void stopTimer(){
        L.d("stopTimer");
        if (mTimer != null) {
            mTimer.cancel();
            mTimer = null;
        }
    }



    class MainTimerTask extends TimerTask {
        public void run() {
            //L.d("MainTimerTask");
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    mTimerCount++;
                    L.d("MainTimerTask timer count  ::; " + mTimerCount);
                    if(checkMaxActionTime()){
                        //TODO 다음 액션 상태로 변경

                        mTimerCount = 0;

                        // send to ChaStateManager
                        HandleUtil.sendMessage(mManChaHandler, Constant.CHARACTER_STATE_TYPE.NEXT_STATE);
                    }

                }
            });
        }
    }

    /**
     * 액션 시간 체크
     * @return
     */
    private boolean checkMaxActionTime(){
        if(mTimerCount > DEFAULT_STATE_TIME){
            return true;
        }

        return false;
    }

}
