package company.jumal.raisethecouple.constant;

/**
 * Created by seeroo_dev on 2018. 3. 12..
 */

public interface ICharacterMenuListId {
    /**
     * 캐릭터 메뉴 버튼 ID 정의
     */

    // 전화걸기
    int INT_FLAG_CHA_MENU_01              =       0x0001;

    // 메뉴 닫기
    int INT_FLAG_CHA_MENU_02              =       0x0002;

    // 캐릭터 숨기기(서비스 종료)
    int INT_FLAG_CHA_MENU_03              =       0x0003;
}
