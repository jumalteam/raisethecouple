package company.jumal.raisethecouple.views.base;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import company.jumal.raisethecouple.callback.IonBackPressed;
import company.jumal.raisethecouple.views.main.MainActivity;

/**
 * Created by jsKim on 2015-11-25.
 * 부모 프래그먼트
 */
public abstract class BaseFragment extends Fragment
{
    /**
     * 현재 Fragment UI의 Container 뷰
     * 프래그먼트에서 public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)함수에서
     * 이용 방법 : container = inflater.infalte(...) 로 레퍼런스 변수 할당함
     */
    protected View v_Container;

    /**
     * 부모 엑티비티
     */
    protected Activity m_AC_Parent = null;

    @Override
    public void onAttach(Activity context)
    {
        super.onAttach(context);

        if (context instanceof Activity)
        {
            m_AC_Parent = (Activity) context;
        }

        ((MainActivity)m_AC_Parent).setOnBackPress(onBackPressed);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return uiInit(inflater, container, savedInstanceState);
    }

    /**
     * 기기 백키
     */
    private IonBackPressed onBackPressed = new IonBackPressed()
    {
        @Override
        public void onBackPress()
        {
            backStackCheck();
        }
    };

    protected View.OnClickListener topBackClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            backStackCheck();
        }
    };

    /**
     * 프래그먼트 갯 수 체크
     */
    private void backStackCheck()
    {
        final FragmentManager manager = ((MainActivity)m_AC_Parent).getSupportFragmentManager();

        // 프래그먼트가 1개 이상 있을 때
        if(manager.getBackStackEntryCount() > 1)
        {
            manager.popBackStackImmediate();
            manager.beginTransaction().commitAllowingStateLoss();
            company.jumal.raisethecouple.utils.manager.fragment.FragmentManager.getInstance().removeFragment(((MainActivity)m_AC_Parent), BaseFragment.this);
        }
        // 마지막 프래그먼트
        else
        {
            m_AC_Parent.finish();
            System.exit(0);
        }
    }



    /**
     * 현재 Fragment에서 해당되는 ID의 UI 객체 리턴
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return
     */
    protected abstract View uiInit(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState);

    protected abstract void onBackPressed();

    /**
     * 현재 Fragment에서 해당되는 ID의 UI 객체 리턴
     * @param _iViewID
     * @return
     */
    protected ImageButton getImageButton(final int _iViewID)
    {
        return (ImageButton)v_Container.findViewById(_iViewID);
    }

    protected EditText getEditText(final int _iViewID)
    {
        return (EditText)v_Container.findViewById(_iViewID);
    }

    protected Button getButton(final int _iViewID)
    {
        return (Button)v_Container.findViewById(_iViewID);
    }

    protected ImageView getImageView(final int _iViewID)
    {
        return (ImageView)v_Container.findViewById(_iViewID);
    }

    protected TextView getTextView(final int _iViewID)
    {
        return (TextView)v_Container.findViewById(_iViewID);
    }

    protected ListView getListView(final int _iViewID)
    {
        return (ListView)v_Container.findViewById(_iViewID);
    }

    protected RelativeLayout getRelativeLayout(final int _iViewID)
    {
        return (RelativeLayout)v_Container.findViewById(_iViewID);
    }

    protected FrameLayout getFrameLayout(final int _iViewID)
    {
        return (FrameLayout)v_Container.findViewById(_iViewID);
    }

    protected ViewPager getViewPager(final int _iViewID)
    {
        return (ViewPager)v_Container.findViewById(_iViewID);
    }

    protected View getView(final int _iViewID)
    {
        return v_Container.findViewById(_iViewID);
    }

    protected ScrollView getScrolView(final int _iViewID)
    {
        return (ScrollView)v_Container.findViewById(_iViewID);
    }

    protected LinearLayout getLinearLayout(final int _iViewID)
    {
        return (LinearLayout)v_Container.findViewById(_iViewID);
    }

    protected ExpandableListView getExpandableListview(final int _iViewID)
    {
        return (ExpandableListView)v_Container.findViewById(_iViewID);
    }

    protected GridView getGrideView(final int _iViewID)
    {
        return (GridView)v_Container.findViewById(_iViewID);
    }

    protected HorizontalScrollView getHorizontalScrollView(final int _iViewID)
    {
        return (HorizontalScrollView)v_Container.findViewById(_iViewID);
    }

    protected WebView getWebView(final int _iViewID)
    {
        return (WebView)v_Container.findViewById(_iViewID);
    }

    protected CheckBox getCheckBox(final int _iViewID)
    {
        return (CheckBox)v_Container.findViewById(_iViewID);
    }

    protected GridView getGridView(final int _iViewID)
    {
        return (GridView)v_Container.findViewById(_iViewID);
    }
}
