package company.jumal.raisethecouple.models;

import android.databinding.BaseObservable;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.util.Patterns;


public class User extends BaseObservable
{
    @NonNull
    private String  mail;

    @NonNull
    private String  pw;

    @NonNull
    private String gender;

    @NonNull
    private String name;

    @NonNull
    private String loca;

    @NonNull
    private String phone;

    public User(@NonNull final String _strEmail, @NonNull final String _strPW, @NonNull final String _gender,
                @NonNull final String _strName, @NonNull final String _strLoca, @NonNull final String _strPhone)
    {
        mail = _strEmail;
        pw = _strPW;
        gender = _gender;
        name = _strName;
        loca = _strLoca;
        phone = _strPhone;

    }

    @NonNull
    public String getMail() {
        return mail;
    }

    public void setMail(@NonNull String mail) {
        this.mail = mail;
        notifyChange();
    }

    @NonNull
    public String getPw() {
        return pw;
    }

    public void setPw(@NonNull String pw) {
        this.pw = pw;
        notifyChange();
    }

    @NonNull
    public String getGender() {
        return gender;
    }

    public void seGender(@NonNull String gender) {
        this.gender = gender;
        notifyChange();
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
        notifyChange();
    }

    @NonNull
    public String getLoca() {
        return loca;
    }

    public void setLoca(@NonNull String loca) {
        this.loca = loca;
        notifyChange();
    }

    @NonNull
    public String getPhone() {
        return phone;
    }

    public void setPhone(@NonNull String phone) {
        this.phone = phone;
        notifyChange();
    }

    public boolean isInputDataValid()
    {
        if(TextUtils.isEmpty(getMail()) == false)
            return false;
        else if(Patterns.EMAIL_ADDRESS.matcher(getMail()).matches() == false)
            return false;
        else if(TextUtils.isEmpty(getPw()) == false)
            return false;
        else if(getPw().length() < 5)
            return false;
        else if(TextUtils.isEmpty(getGender()) == false)
            return false;
        else if(TextUtils.isEmpty(getName()) == false)
            return false;
        else if(TextUtils.isEmpty(getLoca()) == false)
            return false;
        else if(TextUtils.isEmpty(getPhone()) == false)
            return false;
        else if(Patterns.PHONE.matcher(getPhone()).matches() == false)
            return false;

        return true;
    }
}
