package company.jumal.raisethecouple.interfaces;

public interface LoginResultCallback {
    void onSuccess(String msg);
    void onError(String msg);

}
