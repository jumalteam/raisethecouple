package company.jumal.raisethecouple.utils.util;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by seeroo_dev on 2018. 4. 13..
 */

public class AnimationTimer extends Timer{

    private long mFinishVal = 0;
    private long    mAddVal = 0;
    private long mPeriodVal = 0;

    public AnimationTimer(long finishVal){
        this.mAddVal    = 0;
        this.mPeriodVal = 0;
        this.mFinishVal = finishVal;
    }

    public void timerFinish(){
        mFinishVal  = 0;
        mAddVal     = 0;
        mPeriodVal  = 0;
        super.cancel();
    }

    @Override
    public void schedule(TimerTask task, Date time) {
        super.schedule(task, time);
    }

    @Override
    public void schedule(TimerTask task, long delay) {
        super.schedule(task, delay);
    }

    public void schedule(TimerTask task, long finish, long period){
        mFinishVal = finish;
        mPeriodVal = period;
        super.schedule(task, 0, period);
    }


    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public int purge() {
        return super.purge();
    }

    public long getmFinishVal() {
        return mFinishVal;
    }

    public void setmFinishVal(long mFinishVal) {
        this.mFinishVal = mFinishVal;
    }

    public long getmPeriodVal() {
        return mPeriodVal;
    }

    public void setmPeriodVal(long mPeriodVal) {
        this.mPeriodVal = mPeriodVal;
    }

    public long getmAddVal() {
        return mAddVal;
    }

    public void setmAddVal(long mAddVal) {
        this.mAddVal = mAddVal;
    }
}
