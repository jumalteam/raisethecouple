package company.jumal.raisethecouple.callback;

/**
 * Created by jsKim on 2015-11-26.
 */
public interface IonBackPressed
{
    void    onBackPress();
}
