package company.jumal.raisethecouple.utils.manager.fragment;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;

import company.jumal.raisethecouple.views.account_manager.AccountManagerFragment;
import company.jumal.raisethecouple.views.app_manager.AppManagerFragment;
import company.jumal.raisethecouple.views.diary.DiaryFragment;
import company.jumal.raisethecouple.views.etc.ETCFragment;
import company.jumal.raisethecouple.views.login.LogintFragment;
import company.jumal.raisethecouple.views.main.MainFragment;
import company.jumal.raisethecouple.views.moster.MonsterFragment;
import company.jumal.raisethecouple.views.setting.SettingFragment;

/**
 * Created by jsKim on 2015-11-26.
 */
public class FragmentManager
{
    // 로그인
    public  static  final   int INT_FRAG_TYPE_LOGIN             =       50;

    // 메인 프래그
    public  static  final   int INT_FRAG_TYPE_MAIN              =       100;

    // 몬스터 프래그
    public  static  final   int  INT_FRAG_TYPE_MONSTER           =       200;

    // 앱 매니저 프래그
    public  static  final   int  INT_FRAG_TYPE_APP_MANAGER       =       300;

    // 다이어리 프래그
    public  static  final   int  INT_FRAG_TYPE_DIARY             =       400;

    // 기타 프래그
    public  static  final   int  INT_FRAG_TYPE_ETC               =       500;

    // 설정 프래그
    public  static  final   int  INT_FRAG_TYPE_SETTING           =       600;

    // 계정 관리 프래그
    public  static  final   int  INT_FRAG_TYPE_ACCOUNT_MANGER    =       700;


    private static FragmentManager m_Instance = null;

    public synchronized static FragmentManager getInstance()
    {
        if (m_Instance == null)
            m_Instance = new FragmentManager();

        return m_Instance;
    }

    /**
     * 프래그먼트 초기화
     *
     * @param _ac
     * @param _iLayout
     * @param _iFragNum
     * @param _object
     */
    public void replaceFragment(FragmentActivity _ac, int _iLayout, int _iFragNum, Object... _object) {
        FragmentTransaction fragTsaction = _ac.getSupportFragmentManager().beginTransaction();
        Fragment newFragment = getFragmentCheck(_iFragNum, _object);
        fragTsaction.replace(_iLayout, newFragment);
        fragTsaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragTsaction.commitAllowingStateLoss();
    }

    /**
     * 프래그먼트 추가
     *
     * @param _ac
     * @param _iLayout
     * @param _iFragNum
     * @param _object
     */
    public void addFragment(FragmentActivity _ac, int _iLayout, int _iFragNum, Object... _object)
    {
        FragmentTransaction fragTsaction = _ac.getSupportFragmentManager().beginTransaction();
        Fragment newFragment = getFragmentCheck(_iFragNum, _object);
        fragTsaction.add(_iLayout, newFragment);
        fragTsaction.addToBackStack(newFragment.getClass().getName());
        fragTsaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragTsaction.commitAllowingStateLoss();
    }

    /**
     * 프래그먼트 추가
     * @param _ac
     * @param _iLayout
     * @param _frag
     */

    public void addFragment(FragmentActivity _ac, int _iLayout, Fragment _frag) {
        FragmentTransaction fragTsaction = _ac.getSupportFragmentManager().beginTransaction();
        fragTsaction.add(_iLayout, _frag);
        fragTsaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragTsaction.commitAllowingStateLoss();
    }

    public void showFragment(FragmentActivity _ac,Fragment _frag)
    {
        FragmentTransaction fragTsaction = _ac.getSupportFragmentManager().beginTransaction();
        fragTsaction.show(_frag);
        fragTsaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragTsaction.commitAllowingStateLoss();
    }

    public void hideFragment(FragmentActivity _ac,Fragment _frag)
    {
        FragmentTransaction fragTsaction = _ac.getSupportFragmentManager().beginTransaction();
        fragTsaction.hide(_frag);
        fragTsaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        fragTsaction.commitAllowingStateLoss();
    }

    /**
     * 프래그먼트 제거
     *
     * @param _ac
     * @param _frg
     */
    public void removeFragment(FragmentActivity _ac, Fragment _frg) {
        _ac.getSupportFragmentManager().beginTransaction().remove(_frg).commit();
    }

    /**
     * 스택 모두 제거
     * @param _ac
     */
    public void clearBackStack(FragmentActivity _ac)
    {
        final android.support.v4.app.FragmentManager fm = _ac.getSupportFragmentManager();
        fm.popBackStack(null, android.support.v4.app.FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }

    /**
     * 프래그먼트 화면 체크
     *
     * @param _iFargNum
     * @param _object
     * @return
     */
    private Fragment getFragmentCheck(int _iFargNum, Object... _object)
    {
        Fragment newFragment = null;

        switch (_iFargNum)
        {
            // 로그인
            case INT_FRAG_TYPE_LOGIN:
                newFragment =   new LogintFragment();
                break;

            // 메인
            case INT_FRAG_TYPE_MAIN:
                newFragment = new MainFragment();
            break;

            case INT_FRAG_TYPE_MONSTER:
                newFragment = new MonsterFragment();
                break;

            case INT_FRAG_TYPE_APP_MANAGER:
                newFragment = new AppManagerFragment();
                break;

            case INT_FRAG_TYPE_DIARY:
                newFragment = new DiaryFragment();
                break;

            case INT_FRAG_TYPE_ETC:
                newFragment = new ETCFragment();
                break;

            case INT_FRAG_TYPE_SETTING:
                newFragment = new SettingFragment();
                break;

            case INT_FRAG_TYPE_ACCOUNT_MANGER:
                newFragment = new AccountManagerFragment();
                break;
        }

        return newFragment;
    }

}
