package company.jumal.raisethecouple.views.intro;

import android.content.Intent;
import android.os.Bundle;

import company.jumal.raisethecouple.views.base.BaseActivity;
import company.jumal.raisethecouple.views.main.MainActivity;

public class IntroActivity extends BaseActivity
{
    private    static  final   int INT_SPLASH_SLEEP_TIME   =   3000;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        try {
            Thread.sleep(INT_SPLASH_SLEEP_TIME);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void setButterKnife() {

    }
}
