package company.jumal.raisethecouple.utils;

/**
 * Created by seeroo_dev on 2017. 10. 10..
 */

public class Constant
{
    public static class HANDLER_MESSAGE_TYPE {
        public static final int OPEN_MENU = 0x0001;
        public static final int CLOSE_MENU = 0x0002;
    }

    public static class CHARACTER_STATE_TYPE{
        public static final int NEXT_STATE = 0x00001;
    }


}
