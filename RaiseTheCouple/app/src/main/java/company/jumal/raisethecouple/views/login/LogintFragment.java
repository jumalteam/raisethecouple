package company.jumal.raisethecouple.views.login;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import company.jumal.raisethecouple.R;
import company.jumal.raisethecouple.views.base.BaseFragment;

/**
 * Created by Administrator on 2018-02-18.
 * 설정
 */

public class LogintFragment extends BaseFragment
{
    @Override
    protected View uiInit(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        v_Container = inflater.inflate(R.layout.fragment_login, container, false);

        return v_Container;
    }

    @Override
    protected void onBackPressed() {

    }
}
