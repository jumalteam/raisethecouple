package company.jumal.raisethecouple.views.base;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

/**
 * Created by seeroo_dev on 2017. 9. 18..
 */

public abstract class BaseActivity extends FragmentActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setButterKnife();
    }

    //set ButterKnife
    protected abstract void setButterKnife();



    @Override
    public void onResume(){
        super.onResume();
    }



    @Override
    public void onPause(){
        super.onPause();
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
    }




}
