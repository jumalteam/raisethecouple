package company.jumal.raisethecouple.utils.util;

import android.os.Message;

/**
 * Created by seeroo_dev on 2018. 3. 12..
 */

public interface IOnHandlerMessage {
    void handlerMessage(Message msg);
}
