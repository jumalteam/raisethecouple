package company.jumal.raisethecouple.utils;

import android.util.Log;

import company.jumal.raisethecouple.App;

/**
 * Created by seeroo_dev on 2017. 9. 18..
 */

public class L
{
    private static final String TAG = L.class.getSimpleName();
    private static boolean LOG_DEBUG = true;


    /** Log Level Error */
    public static final void e(String message){
        if(App.DEBUG) Log.e(getLocalTag(), message);
    }

    /** Log Level Warning */
    public static final void w(String message){
        if(App.DEBUG) Log.w(getLocalTag(), message);
    }

    /** Log Level Information */
    public static final void i(String message){
        if(App.DEBUG) Log.i(getLocalTag(), message);
    }

    /** Log Level Debug */
    public static final void d(String message){
        if(App.DEBUG) Log.d(getLocalTag(), message);
    }

    /** Log Level verbose */
    public static final void v(String message){
        if(App.DEBUG) Log.v(getLocalTag(), message);
    }


    /** Building Tag */
    private static String getLocalTag() {
        StackTraceElement ste = Thread.currentThread().getStackTrace()[4];
        StringBuilder sb = new StringBuilder();
        sb.append(ste.getFileName().replace(".java", ""));
        sb.append("::");
        sb.append(ste.getMethodName());

        return sb.toString();
    }




    public static boolean isLogDebug() {
        return LOG_DEBUG;
    }

    public static void setLogDebug(boolean logDebug) {
        LOG_DEBUG = logDebug;
    }
}
