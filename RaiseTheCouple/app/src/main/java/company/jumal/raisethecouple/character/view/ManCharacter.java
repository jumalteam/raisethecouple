package company.jumal.raisethecouple.character.view;

import android.content.Context;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import company.jumal.raisethecouple.utils.L;
import company.jumal.raisethecouple.utils.entity.ClickEntity;

import static android.content.Context.WINDOW_SERVICE;

/**
 * Created by seeroo_dev on 2017. 10. 10..
 */

public class ManCharacter extends BaseCharacter implements View.OnTouchListener, View.OnLongClickListener {

    private Context         mContext        = null;
    private GestureDetector mDectector      = null;
    private ClickEntity     clickLocation   = null;

    public ManCharacter(Context context) {
        super(context);
        this.mContext = context;
        clickLocation = new ClickEntity();
        mDectector = new GestureDetector(mContext, new MyGestureListener());
        setOnTouchListener(this);
    }

    @Override
    public boolean onLongClick(View view) {
        L.d("onLongClick----------------");
        return false;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        mDectector.onTouchEvent(motionEvent);

        int x = getLeft();
        int y = getRight();

        L.d("x ::: " + x);
        L.d("y ::: " + y);

        switch (motionEvent.getAction()){
            case MotionEvent.ACTION_DOWN:
                //사용자 터치 다운
                actionDown(motionEvent);
                break;

            case MotionEvent.ACTION_MOVE:
                //사용자 터치 무브
                actionMove(motionEvent);
                break;

            case MotionEvent.ACTION_UP:
                //사용자 터치 업
                actionUp(motionEvent);
                break;

            default:
                break;
        }

        return false;
    }


    public void actionDown(MotionEvent motionEvent) {
        clickLocation.setActionDown(motionEvent);
        clickLocation.setPrev_x(getmParams().x);
        clickLocation.setPrev_y(getmParams().y);
    }

    public void actionMove(MotionEvent motionEvent) {

        //이동위치 저장
        int x = (int)(motionEvent.getRawX() - clickLocation.getStart_x());
        int y = (int)(motionEvent.getRawY() - clickLocation.getStart_y());

        //터치해서 이동한 만큼 이동
        getmParams().x = (int)clickLocation.getPrev_x() + x;
        getmParams().y = (int)clickLocation.getPrev_y() + y;

        updateWindowView();
    }

    public void actionUp(MotionEvent motionEvent) {}

    private void updateWindowView(){
        //최상위(윈도우)레이아웃에 캐릭터가 변경된 사항 update
        WindowManager mWindowManager = (WindowManager) mContext.getSystemService(WINDOW_SERVICE);
        if (mWindowManager != null) {
            mWindowManager.updateViewLayout(this, getmParams());
        }

        //수정
    }


    /**
     * 캐릭터 메뉴 노출을 위한 메시지 전송
     */
    private void reqOpenChaMenu(){
    }

    private void reqCloseChaMenu(){

    }


    class MyGestureListener extends GestureDetector.SimpleOnGestureListener{
        @Override
        public boolean onSingleTapConfirmed(MotionEvent motionEvent) {
            L.d("onSingleTapConfirmed");
            return false;
        }

        @Override
        public boolean onDoubleTap(MotionEvent motionEvent) {
            L.d("onDoubleTap");

            // 더블텝 menu 노출 요청
            reqOpenChaMenu();

            return false;
        }

        @Override
        public boolean onDoubleTapEvent(MotionEvent motionEvent) {
            L.d("onDoubleTapEvent");
            return false;
        }
    }


}
