package company.jumal.raisethecouple.views.main;

import android.os.Bundle;

import butterknife.ButterKnife;
import company.jumal.raisethecouple.R;
import company.jumal.raisethecouple.callback.IonBackPressed;
import company.jumal.raisethecouple.utils.manager.fragment.FragmentManager;
import company.jumal.raisethecouple.views.base.BaseActivity;

public class MainActivity extends BaseActivity
{
    public static final String TAG = MainActivity.class.getSimpleName();

    // 백키 이벤트 받을 콜백
    private IonBackPressed m_BackCallback = null;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setWindowFlag();

        // 메인 프래그먼트 추가
        FragmentManager.getInstance().addFragment(MainActivity.this, R.id.fl_main_frag, FragmentManager.INT_FRAG_TYPE_LOGIN, "");
    }

    private void setWindowFlag(){
//        getWindow().setFlags(
//                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
//                        WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
//                        WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON,
//                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
//                        WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
//                        WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
    }


    @Override
    protected void setButterKnife() {
        ButterKnife.bind(this);
    }


    /**
     * 콜백 등록
     * @param _callback
     */
    public void setOnBackPress(IonBackPressed _callback)
    {
        m_BackCallback = _callback;
    }

    @Override
    public void onBackPressed()
    {
        if(m_BackCallback != null)
            m_BackCallback.onBackPress();
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


}
