package company.jumal.raisethecouple.views.diary;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import company.jumal.raisethecouple.R;
import company.jumal.raisethecouple.views.base.BaseFragment;

/**
 * Created by Administrator on 2018-02-18.
 * 다이어리
 */

public class DiaryFragment extends BaseFragment
{
    @Override
    protected View uiInit(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        v_Container = inflater.inflate(R.layout.fragment_diary, container, false);

        return v_Container;
    }

    @Override
    protected void onBackPressed() {

    }
}
