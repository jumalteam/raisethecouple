package company.jumal.raisethecouple.views.account_manager;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import company.jumal.raisethecouple.R;
import company.jumal.raisethecouple.views.base.BaseFragment;

/**
 * Created by Administrator on 2018-02-18.
 * 계정 관리
 */
public class AccountManagerFragment extends BaseFragment
{
    @Override
    protected View uiInit(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        v_Container = inflater.inflate(R.layout.fragment_account_manager, container, false);

        return v_Container;
    }

    @Override
    protected void onBackPressed() {

    }
}
