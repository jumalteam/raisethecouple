package company.jumal.raisethecouple.utils.manager.permission;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;

import company.jumal.raisethecouple.R;

/**
 * Created by seeroo_dev on 2017. 9. 18..
 */

public class PermissionManager {
    private static final PermissionManager ourInstance = new PermissionManager();

    public static final int REQ_PERMISSION_CODE_SYSTEM_ALERT_WINDOW = 1001;

    public static PermissionManager getInstance() {
        return ourInstance;
    }

    private PermissionManager() {
    }


    /**
     * permission에 해당하는 권한을 앱이 가지고 있는지 체크
     * @param context
     * @param permission
     * @return
     *
     * Manifest.permission.SYSTEM_ALERT_WINDOW
     */
    public boolean chkPermission(Context context, String permission ) {
        int permissionCheck = context.checkSelfPermission(permission);
        if(permissionCheck == PackageManager.PERMISSION_DENIED){
            //권한 없음
            return false;
        }else{
            return true;
        }
    }


    /**
     * 해당 권한 요청
     * @param activity
     * @param reqPermissionStr
     */
    public void reqPermission(Activity activity, String reqPermissionStr, DialogInterface.OnClickListener listener, int reqCode){

        //권한이 필요한 이유를 설명해야 하는지 체크
        if(activity.shouldShowRequestPermissionRationale(reqPermissionStr)){
            // 해당권한 요청 이유 설명

            AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(activity);
            dialogBuilder.setMessage(R.string.dialog_req_permission);
            dialogBuilder.setTitle(R.string.app_name);
            dialogBuilder.setPositiveButton(R.string.ok, listener);
            dialogBuilder.setNegativeButton(R.string.cancel, null);
            AlertDialog alert = dialogBuilder.create();
            alert.show();

        }else{
            // 해당 권한 요청
            activity.requestPermissions(new String[]{reqPermissionStr}, reqCode);
        }

    }


    /**
     * 윈도우 오버레이 권한 체크
     * @param context
     * @return
     */
    public boolean checkOverlayWindowService(Context context){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                && !Settings.canDrawOverlays(context)){
            return false;
        }else{
            return true;
        }
    }

    public void onObtainingPermissionOverlayWindow(Activity activity){
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + activity.getPackageName()));
            activity.startActivityForResult(intent, REQ_PERMISSION_CODE_SYSTEM_ALERT_WINDOW);
        }
    }

}
