package company.jumal.raisethecouple.services;

import android.app.Service;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.Random;

import company.jumal.raisethecouple.R;
import company.jumal.raisethecouple.character.view.BaseCharacter;
import company.jumal.raisethecouple.character.view.ManCharacter;
import company.jumal.raisethecouple.constant.ICharacterMenuListId;
import company.jumal.raisethecouple.utils.Constant;
import company.jumal.raisethecouple.utils.L;
import company.jumal.raisethecouple.utils.util.IOnHandlerMessage;
import company.jumal.raisethecouple.utils.util.WeakRefHandler;

public class AlwaysOnTopService extends Service implements IOnHandlerMessage{

    private static final String TAG = AlwaysOnTopService.class.getSimpleName();


    private LinearLayout ll_popupView     = null;
    private BaseCharacter mCharacter      = null;
    private WindowManager mWindowManager  = null;

    // Memory leak 방지하기 위하여 Handler를 재정의하여 사용
    private WeakRefHandler mHandler = new WeakRefHandler(this);


    public AlwaysOnTopService() {
        L.d("log");
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate(){
        super.onCreate();
        L.d("onCreate-----");

        initVariable();
        InitLayout();
    }


    @Override
    public void handlerMessage(Message msg) {
        L.d("AlwaysOnTopService mHandler message::: " + msg.toString());

        switch(msg.what){
            case Constant.HANDLER_MESSAGE_TYPE.OPEN_MENU:
                createChaMenuView();
                break;

            case Constant.HANDLER_MESSAGE_TYPE.CLOSE_MENU:
                closeChaMenuView();
                break;


            //TODO MESSAGE 정의

            default:
                break;
        }
    }

    private void closeChaMenuView() {
        chaMenuClose();
    }

    /**
     * Create the Character Menu View
     */
    private void createChaMenuView() {
        try {
            ll_popupView = new LinearLayout(this);
            ll_popupView.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            ll_popupView.setOrientation(LinearLayout.VERTICAL);

            Button callButton = new Button(this);
            callButton.setText(R.string.call_cha_menu);
            callButton.setId(ICharacterMenuListId.INT_FLAG_CHA_MENU_01);
            callButton.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            callButton.setOnClickListener(mMenuClickListener);
            ll_popupView.addView(callButton);

            Button closeMenuButton = new Button(this);
            closeMenuButton.setText(R.string.close_cha_menu);
            closeMenuButton.setId(ICharacterMenuListId.INT_FLAG_CHA_MENU_02);
            closeMenuButton.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            closeMenuButton.setOnClickListener(mMenuClickListener);
            ll_popupView.addView(closeMenuButton);

            Button closeButton = new Button(this);
            closeButton.setText(R.string.close_cha_hide);
            closeButton.setId(ICharacterMenuListId.INT_FLAG_CHA_MENU_03);
            closeButton.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            closeButton.setOnClickListener(mMenuClickListener);
            ll_popupView.addView(closeButton);

            addViewInWindow(ll_popupView);
        }catch (Exception e){
            L.e("createChaMenuView Exception " + e.toString());
        }
    }


    /**
     * Character menu Click Listener
     */
    View.OnClickListener mMenuClickListener = new View.OnClickListener(){

        @Override
        public void onClick(View view) {
            switch (view.getId()){
                case ICharacterMenuListId.INT_FLAG_CHA_MENU_01:
                    // 전화걸기

                    //TODO TEST Phone Number
                    call("tel:01033081560");
                    chaMenuClose();
                    break;
                case ICharacterMenuListId.INT_FLAG_CHA_MENU_02:

                    // 닫기
                    chaMenuClose();
                    break;

                case ICharacterMenuListId.INT_FLAG_CHA_MENU_03:

                    //service를 종료 한다.
                    stopService();
                    break;

                default:
                    break;
            }
        }
    };


    /**
     * Call
     * @param phoneNum
     */
    private void call(String phoneNum){
        if(!TextUtils.isEmpty(phoneNum)){
            try {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                //intent.setData(Uri.parse("tel:phone number"));

                //temp code
                intent.setData(Uri.parse("tel:01033081560"));
                startActivity(intent);
                return;

            }catch (Exception e){
                L.e("call exception e : " + e.toString());
            }
        }else{
            L.e("phone number is null");
        }

        Toast.makeText(this, R.string.plz_check_the_phone_app, Toast.LENGTH_SHORT).show();
    }


    /**
     * Close the Menu view
     */
    private void chaMenuClose(){
        if(ll_popupView != null){
            ll_popupView.removeAllViews();
        }

        if(mWindowManager != null) {
            mWindowManager.removeView(ll_popupView);
            ll_popupView = null;
        }
    }

    private void initVariable(){

    }

    private void InitLayout(){
        initCharacterView();
    }


    /**
     * 캐릭터 생성
     */
    private void initCharacterView(){
        mCharacter = new ManCharacter(this);
        mCharacter.setDefaulSettingParams();
        mCharacter.setDefaultResource();

        addChaInWindow(mCharacter);

        final TranslateAnimation animation = new TranslateAnimation(mCharacter.getmParams().x, randMovePos(),
                mCharacter.getmParams().y, randMovePos());          //  new TranslateAnimation(xFrom,xTo, yFrom,yTo)
        animation.setDuration(5000);  // animation duration
        animation.setRepeatCount(5);  // animation repeat count
        animation.setRepeatMode(2);   // repeat animation (left to right, right to left )
        //animation.setFillAfter(true);

        //mCharacter.startAnimation(animation);  // start animation
        mCharacter.setAnimation(animation);
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                animation.start();

            }
        });
    }

    public float randMovePos(){
        int nextPos = new Random().nextInt(100);

        boolean isNegative = new Random().nextBoolean();
        if(!isNegative){
            nextPos = -1 * nextPos;
        }

        return nextPos;
    }

    private void addViewInWindow(View view){
        try {
            if (null == mWindowManager) {
                mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
            }

            WindowManager.LayoutParams mParams = new WindowManager.LayoutParams(
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.TYPE_PHONE,          // 항상 최 상단 터치 이벤트 받을 수 있음
                    //WindowManager.LayoutParams.TYPE_APPLICATION_PANEL,
                    //WindowManager.LayoutParams.TYPE_TOAST,
//                WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY,
                    WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN,  // 포커스를 가지지 않음
                    PixelFormat.TRANSLUCENT);                       // 투명
            mParams.gravity = Gravity.CENTER_HORIZONTAL | Gravity.TOP;           // 중앙

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                mParams.type = WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY;
            }

            mParams.x = mCharacter.getmParams().x;
            mParams.y = mCharacter.getmParams().y;

            mWindowManager.addView(view, mParams);
        }catch (Exception e){
            L.e("addViewInWindow e :: " + e.toString());
        }
    }

    private void addChaInWindow(BaseCharacter view){
        try {

            //add window view
            if(null == mWindowManager) {
                mWindowManager = (WindowManager) getSystemService(WINDOW_SERVICE);
            }

            mWindowManager.addView(view, view.getmParams());
        }catch (Exception e){
            L.e("addCharacterInWindow :: " + e.toString());
        }
    }


    @Override
    public void onDestroy(){
        L.d("log");

        try {

            if(mCharacter != null) {
                mWindowManager.removeView(mCharacter);
            }

            if(ll_popupView != null) {
                mWindowManager.removeView(ll_popupView);
            }

            stopService();

        }catch (Exception e){
            L.e("ondestroy error : " + e.toString());
        }

        super.onDestroy();
    }

    private void stopService(){
        Intent intent = new Intent(this, AlwaysOnTopService.class);
        stopService(intent);
    }

}
