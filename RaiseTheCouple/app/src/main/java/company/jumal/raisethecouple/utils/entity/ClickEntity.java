package company.jumal.raisethecouple.utils.entity;

import android.view.MotionEvent;

/**
 * Created by seeroo_dev on 2017. 9. 18..
 */

public class ClickEntity {

    private float start_x;
    private float start_y;

    private float prev_x;
    private float prev_y;

    public ClickEntity(){

    }


    public float getStart_x() {
        return start_x;
    }

    public float getStart_y() {
        return start_y;
    }

    public float getPrev_x() {
        return prev_x;
    }

    public float getPrev_y() {
        return prev_y;
    }

    public void setStart_x(float start_x) {
        this.start_x = start_x;
    }

    public void setStart_y(float start_y) {
        this.start_y = start_y;
    }

    public void setPrev_x(float prev_x) {
        this.prev_x = prev_x;
    }

    public void setPrev_y(float prev_y) {
        this.prev_y = prev_y;
    }

    public void setActionDown(MotionEvent motionEvent) {
        setStart_x(motionEvent.getRawX());
        setStart_y(motionEvent.getRawY());
    }
}
