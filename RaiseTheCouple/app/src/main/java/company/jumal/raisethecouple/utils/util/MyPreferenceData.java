package company.jumal.raisethecouple.utils.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import java.util.Iterator;
import java.util.Map;

/**
 * Created by riverain on 2016-03-24.
 */
public class MyPreferenceData {

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;




    //CONSTANT DATA KEY SETTING -------------------------------------------------
    private static final String APP_KEY = "appkey";

    //CONSTANT DATA KEY SETTING -------------------------------------------------

    public MyPreferenceData(Context context){
        preferences = PreferenceManager.getDefaultSharedPreferences(context);
        editor = preferences.edit();
    }

    public String getAllPreferenceData() {

        Map<String, ?> map = preferences.getAll();
        Iterator<?> iterator= map.entrySet().iterator();
        while(iterator.hasNext())
        {
            Map.Entry entry =(Map.Entry)iterator.next();
        }
        return null;
    }

    public void commit() { editor.commit(); }

    public synchronized void setAllClearData(){
        editor.clear();
        commit();
    }


    public synchronized String getAppKeyData(){
        return preferences.getString(APP_KEY, null);
    }

    public synchronized void setAppKeyData(String appKey){
        editor.putString(APP_KEY, appKey);
    }

}
