package company.jumal.raisethecouple.views.main;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import company.jumal.raisethecouple.R;
import company.jumal.raisethecouple.utils.manager.fragment.FragmentManager;
import company.jumal.raisethecouple.views.base.BaseFragment;

/**
 * Created by Administrator on 2018-02-18.
 * 메인
 */

public class MainFragment extends BaseFragment
{
    // 메뉴 종류 관련 Begin
    private static final int INT_MENU_MONSTER           =   0;
    private static final int INT_MENU_APP_MANAGER       =   1;
    private static final int INT_MENU_DIARY             =   2;
    private static final int INT_MENU_ETC               =   3;
    private static final int INT_MENU_SETTING           =   4;
    private static final int INT_MENU_ACCOUNT_MANAGER   =   5;
    private static final int INT_MENU_CNT               =   6;

    private final Button[] BT_MENU = new Button[INT_MENU_CNT];
    // 메뉴 종류 관련 End

    @Override
    protected View uiInit(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        v_Container = inflater.inflate(R.layout.fragment_main, container, false);

        for(int i = 0; i < INT_MENU_CNT; i++)
        {
            int resID = getResources().getIdentifier("bt_main_menu_" + i, "id", getActivity().getPackageName());
            BT_MENU[i] = getButton(resID);
            BT_MENU[i].setOnClickListener(m_MenuClickListener);
        }

        return v_Container;
    }



    /**
     * 메뉴 클릭 리스너
     */
    private View.OnClickListener m_MenuClickListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View view)
        {
            for(int i = 0; i < INT_MENU_CNT; i++)
            {
                if(BT_MENU[i] == view)
                {
                    clickMenu(i);
                    break;
                }
            }

        }
    };



    /**
     * 메뉴 클릭 시 액션
     * @param _iType
     */
    private void clickMenu(int _iType)
    {
        switch (_iType)
        {
            case INT_MENU_MONSTER:
                // 몬스터
                FragmentManager.getInstance().addFragment(getActivity(), R.id.fl_main_frag, FragmentManager.INT_FRAG_TYPE_MONSTER);
            break;

            case INT_MENU_APP_MANAGER:
                // 앱 매니저
                FragmentManager.getInstance().addFragment(getActivity(), R.id.fl_main_frag, FragmentManager.INT_FRAG_TYPE_APP_MANAGER);
            break;

            case INT_MENU_DIARY:
                // 다이어리
                FragmentManager.getInstance().addFragment(getActivity(), R.id.fl_main_frag, FragmentManager.INT_FRAG_TYPE_DIARY);
            break;

            case INT_MENU_ETC:
                // 기타
                FragmentManager.getInstance().addFragment(getActivity(), R.id.fl_main_frag, FragmentManager.INT_FRAG_TYPE_ETC);
            break;

            case INT_MENU_SETTING:
                // 설정
                FragmentManager.getInstance().addFragment(getActivity(), R.id.fl_main_frag, FragmentManager.INT_FRAG_TYPE_SETTING);
            break;

            case INT_MENU_ACCOUNT_MANAGER:
                // 매니저
                FragmentManager.getInstance().addFragment(getActivity(), R.id.fl_main_frag, FragmentManager.INT_FRAG_TYPE_ACCOUNT_MANGER);
            break;
        }
    }

    @Override
    protected void onBackPressed() {

    }
}
