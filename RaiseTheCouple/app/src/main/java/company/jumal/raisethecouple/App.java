package company.jumal.raisethecouple;

import android.app.Application;

import company.jumal.raisethecouple.utils.L;

/**
 * Created by seeroo_dev on 2017. 9. 18..
 */

public class App extends Application
{
    public static final String TAG = App.class.getSimpleName();

    /** Application Debug Mode */
    public static boolean DEBUG = true;

    //Global variable

    @Override
    public void onCreate()
    {
        super.onCreate();

        //set DEBUG MODE
        DEBUG = true;
        L.setLogDebug(DEBUG);

    }


}
