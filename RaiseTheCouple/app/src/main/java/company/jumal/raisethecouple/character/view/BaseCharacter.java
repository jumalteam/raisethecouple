package company.jumal.raisethecouple.character.view;

import android.content.Context;
import android.graphics.PixelFormat;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.WindowManager;

import company.jumal.raisethecouple.R;

/**
 * Created by seeroo_dev on 2017. 9. 29..
 */

public class BaseCharacter extends android.support.v7.widget.AppCompatImageView{

    private WindowManager.LayoutParams  mParams         = null;

    public BaseCharacter(Context context) {
        super(context);
    }

    public BaseCharacter(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }


    public void setDefaulSettingParams(){
        mParams = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_PHONE,          // 항상 최 상단 터치 이벤트 받을 수 있음
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,  // 포커스를 가지지 않음
                PixelFormat.TRANSLUCENT);                       // 투명

        mParams.gravity = Gravity.CENTER_HORIZONTAL | Gravity.TOP;           // 위치 왼쪽 상단
    }

    public void setmParams(WindowManager.LayoutParams mParams) {
        this.mParams = mParams;
    }

    public WindowManager.LayoutParams getmParams() {
        return mParams;
    }

    public void setDefaultResource(){
        setImageResource(R.mipmap.ic_launcher);
    }


}
